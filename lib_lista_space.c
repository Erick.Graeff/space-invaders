#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_lista_space.h"

void imprime_lista (t_lista *l_a, t_lista *l_t, t_barreira *b, t_tiro *t) {
	if (lista_vazia (l_a))
		return;
	erase ();
	imprime_bordas (l_a);
	imprime_alien (l_a, t);
	wattron(stdscr, COLOR_PAIR(2));
	imprime_terra (l_t, b);
	refresh ();
}

void imprime_bordas (t_lista *l) {
	int i, j;

	/* insere bordas horizontais */
	j = 0;
	for (i = 0; i <= l->lin_max; i++) {
		move (i, j);
		addch (BORDA_HORIZONTAL [0]);
	}
	j = l->col_max;
	for (i = 0; i <= l->lin_max; i++) {
		move (i,j);
		addch (BORDA_HORIZONTAL [0]);
	}

	/* insere bordas verticais */
	i = 0;
	for (j = 0; j <= l->col_max; j++) {
		move (i, j);
		addch (BORDA_VERTICAL [0]);
	}

	i = l->lin_max;
	for (j = 0; j <= l->col_max; j++) {
		move (i, j);
		addch (BORDA_VERTICAL [0]);
	}
}

void imprime_alien (t_lista *l, t_tiro *t) {
	int lin, col, k;

	inicializa_atual_inicio (l);
	wattron(stdscr, COLOR_PAIR(1));
	while (consulta_item_atual (l)) {
		k = 0;

		if (l->atual->estado == MORRENDO) {
			for (lin = l->atual->lin; lin < l->atual->lin + 3; lin++)
				for (col = l->atual->col; col < l->atual->col + 5; col++) {
					move (lin, col);
					addch (EXPLOSAO [k]);
					k++;
				}
			l->atual->estado = MORTO;
			remove_item_atual (l);
		}

		else if (l->atual->tipo == 1 && l->atual->alien == 1) {
			for (lin = l->atual->lin; lin < l->atual->lin + 3; lin++)
				for (col = l->atual->col; col < l->atual->col + 5; col++) {
					move (lin, col);
					addch (ALIEN_1 [k]);
					k++;
				}
		}

		else if (l->atual->tipo == 1 && l->atual->alien == 2) {
			for (lin = l->atual->lin; lin < l->atual->lin + 3; lin++)
				for (col = l->atual->col; col < l->atual->col + 5; col++) {
					move (lin, col);
					addch (ALIEN_2 [k]);
					k++;
				}
		}

		else if (l->atual->tipo == 1 && l->atual->alien == 3) {
			for (lin = l->atual->lin; lin < l->atual->lin + 3; lin++)
				for (col = l->atual->col; col < l->atual->col + 5; col++) {
					move (lin, col);
					addch (ALIEN_3 [k]);
					k++;
				}
		}

		else if (l->atual->tipo == 1 && l->atual->alien == 4) {
			for (lin = l->atual->lin; lin < l->atual->lin + 3; lin++)
				for (col = l->atual->col; col < l->atual->col + 9; col++) {
					move (lin, col);
					addch (NAVE_MAE [k]);
					k++;
				}
		}

		incrementa_atual (l);
	}
	wattron(stdscr, COLOR_PAIR(3));
	imprime_tiros (t);
}

void imprime_terra (t_lista *l, t_barreira *b) {
	int lin, col, k;

	inicializa_atual_inicio (l);
	while (consulta_item_atual (l)) {
		k = 0;
		/* caso seja o canhao */
		if (l->atual->tipo == 3) {
			if (l->atual->estado == MORRENDO) {
				for (lin = l->atual->lin; lin < l->atual->lin + 2; lin++)
					for (col = l->atual->col; col < l->atual->col + 5; col++) {
						move (lin, col);
						addch (EXPLOSAO [k]);
						k++;
					}
				endwin ();
				clear ();
				printf ("Voce falhou e os aliens conseguiram invadir a Terra\n");
				exit (0);
			}
			for (lin = l->atual->lin; lin < l->atual->lin + 2; lin++)
				for (col = l->atual->col; col < l->atual->col + 5; col++) {
					move (lin, col);
					addch (CANHAO [k]);
					k++;
				}
		}

		/* caso seja um tiro */
		else if (l->atual->tipo == 4) {
			if (l->atual->lin == 0)
				remove_item_atual (l);
			else {
				lin = l->atual->lin;
				col = l->atual->col;
				move (lin, col);
				addch (TIRO [0]);
				l->atual->lin--;
			}
		}
		incrementa_atual (l);
	}
	imprime_barreira (b);
}

int inicializa_lista (t_lista *l) {
	t_nodo *sent_ini, *sent_fim;

	sent_ini = (t_nodo *) malloc (sizeof (t_nodo));
	if (sent_ini == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}
	sent_fim = (t_nodo *) malloc (sizeof (t_nodo));
	if (sent_fim == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}

	l->ini = sent_ini;
	l->fim = sent_fim;
	sent_ini->prox = l->fim;
	sent_fim->prev = l->ini;
	sent_ini->prev = NULL;
	sent_fim->prox = NULL;
	l->atual = NULL;
	l->tamanho = 0;
	return 1;
}

int lista_vazia (t_lista *l) {
	if (!l->tamanho)
		return 1;
	return 0;
}

void destroi_lista (t_lista *l) {
	if (l->tamanho == 0 && l->ini == NULL && l->fim == NULL && l->atual == NULL) {
		printf ("Lista já foi destruída\n");
		return;
	}

	t_nodo *aux = l->ini;
	l->atual = aux->prox;

	while (l->atual != l->fim->prox) {
		free (aux);
		aux = l->atual;
		l->atual = l->atual->prox;
	}
	free (aux);
	l->ini = NULL;
	l->fim = NULL;
	l->atual = NULL;
	l->tamanho = 0;
}

int tamanho_lista (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	return l->tamanho;
}

int insere_fim_lista (int item, t_lista *l) {

	t_nodo * new;
	new = (t_nodo *) malloc (sizeof (t_nodo));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}

	new->tipo = item;
	l->atual = l->fim->prev;
	new->prev = l->atual;
	new->prox = l->fim;
	l->atual->prox = new;
	l->fim->prev = new;
	new->estado = VIVO;
	l->tamanho++;
	return 1;
}

int inicializa_atual_inicio (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->ini->prox;
	return 1;
}

int inicializa_atual_fim (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	l->atual = l->fim->prev;
	return 1;
}

void incrementa_atual (t_lista *l) {
	if (lista_vazia (l))
		return;

	if (l->atual == l->fim)
		return;

	l->atual = l->atual->prox;
}

void decrementa_atual (t_lista *l) {
	if (lista_vazia (l))
		return;

	if (l->atual->prev == l->ini)
		return;

	l->atual = l->atual->prev;
}

int consulta_item_atual (t_lista *l) {
	if (lista_vazia (l))
		return 0;

	if (l->atual == NULL)
		return 0;

	if (l->atual == l->ini || l->atual == l->fim)
		return 0;

	return 1;
}

int remove_item_atual (t_lista *l) {
	if (lista_vazia(l))
		return 0;

	if (l->atual == NULL)
		return 0;

	if (l->atual == l->ini || l->atual == l->fim)
		return 0;

	t_nodo *aux = l->atual;
	l->atual->prox->prev = l->atual->prev;
	l->atual->prev->prox = l->atual->prox;
	free (aux);
	l->tamanho--;
	return 1;
}