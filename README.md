# Space Invaders

Neste projeto é implementado uma versão texto do jogo [Space Invaders](https://pt.wikipedia.org/wiki/Space_Invaders) na linguagem C utilizando *listas duplamente encadeadas*.


O objetivo do jogo é usar um canhão para destruir os alienígenas antes que eles cheguem ao chão. O canhão está localizado na parte inferior da tela. Ele é controlado pelo jogador que pode atirar ou movê-lo lateralmente.

O alienígenas são de 3 tipos diferentes e estão organizados em 5 linhas e 11 colunas. Eles se movem em bloco de um lado a outro da tela. Quando o bloco chega ao limite da tela, ele desce uma linha e segue na direção contrária até a outra borda da tela. A cada linha que o bloco desce a velocidade de seu movimento lateral aumenta. Caso algum alienígena atinja o chão ou o canhão o jogo termina.

Os alienígenas lançam aleatoriamente bombas, que caem em linha
reta. Caso o canhão seja atingido por uma bomba o jogo termina.

Entre os alienígenas e o canhão existem 4 barreiras que protegem o
canhão das bombas que caem. Caso uma bomba ou um tiro atinja uma
barreira, parte dela é destruída. Caso um alienígena colida com uma
barreira, parte da barreira é distruída e o alienígena segue com seu
movimento.

A cada vez que todos os alienígenas são eliminados, o jogo reinicia
com uma velocidade mais rápida.