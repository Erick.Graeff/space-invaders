CC = gcc
CFLAGS = -Wall

OBJ = space_invaders.o lib_space.o lib_barreira.o lib_lista_space.o lib_tiro.o
LIB = lib_space.h lib_barreira.h lib_lista_space.h lib_tiro.h
FONTE = space_invaders.c lib_space.c lib_barreira.c lib_lista_space.c lib_tiro.c

all: space_invaders

# arquivo executavel
space_invaders: $(FONTE) $(LIB) lib_define.h
	gcc -Wall -o space_invaders $(FONTE) -lncurses

# remove arquivos temporários
clean:
	-rm -f *~

# remove tudo o que nao for
# o codigo fonte original
purge: clean
	-rm -f space_invaders