#ifndef __LIB_SPACE__
#define __LIB_SPACE__

#include "lib_lista_space.h"

void inicializa_ncurses ();

void recebe_input (int key, t_lista *l_a, t_lista *l_t, t_barreira *b);

void atira (t_lista *l_t);

void mv_canhao_dir (t_lista *l);

void mv_canhao_esq (t_lista *l);

int tamanho_jogo (t_lista *l_a, t_lista *l_t, t_tiro *t);

void list_alien_1 (t_lista *l, int x, int *y, int v);

void list_alien_2 (t_lista *l, int x, int *y, int v);

void list_alien_3 (t_lista *l, int x, int *y, int v);

void list_t (t_lista *l, t_barreira *b, int x, int y);

void prepara_jogo (t_lista *l_alien, t_lista *l_terra, t_barreira *b);

int desce_linha (t_lista *l, int *cont);

int ultimo_alien (t_lista *l);

int primeiro_alien (t_lista *l);

int ultima_linha (t_lista *l);

int move_alien (t_lista *l, int sentido, int *cont, /* int *cont_mae,  */int *desce);

void muda_vel_alien (t_lista *l, int *u);

void testa_colisao (t_lista *l_a, t_lista *l_t, t_barreira *b, t_tiro *t, int *pont);

int colide_alien_tiro (t_nodo *alien, t_nodo *tiro);

int colide_barreira_tiro (t_barreira *b, t_nodo *tiro);

void tiro_alien (t_lista *l, t_tiro *t);

void colide_alien_barreira (t_nodo *alien, t_barreira *b);

int colide_bomba_barreira (t_nodo_t *tiro, t_barreira *b);

int colide_bomba_canhao (t_nodo_t *tiro, t_nodo *canhao);

/* void nave_mae (t_lista *l); */

#endif