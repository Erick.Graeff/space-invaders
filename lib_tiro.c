#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_tiro.h"

void imprime_tiros (t_tiro *t) {
    if (lista_tiro_vazia (t))
        return;

    int i, j;
	inicializa_atual_inicio_t (t);
    t->cont++;
	while (consulta_atual_tiro (t)) {
        if (t->ini->prox->lin == t->lin_max)
            remove_atual_tiro (t);
        else if (t->cont % VELOCIDADE_TIRO == 0){
            i = t->atual->lin;
            j = t->atual->col;
            move (i, j);
            addch (TIRO_ALIEN [0]);
            t->atual->lin++;
        }
        incrementa_atual_tiro (t);
	}
}

int inicializa_tiro (t_tiro *t) {
	t_nodo_t *sent_ini, *sent_fim;

	sent_ini = (t_nodo_t *) malloc (sizeof (t_nodo_t));
	if (sent_ini == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}
	sent_fim = (t_nodo_t *) malloc (sizeof (t_nodo_t));
	if (sent_fim == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}

	t->ini = sent_ini;
	t->fim = sent_fim;
	sent_ini->prox = t->fim;
	sent_fim->prev = t->ini;
	sent_ini->prev = NULL;
	sent_fim->prox = NULL;
	t->atual = NULL;
    t->cont = 0;
	t->tamanho = 0;
	return 1;
}

void destroi_list_tiro (t_tiro *t) {
    if (t->ini == NULL && t->fim == NULL && t->atual == NULL) {
		printf ("Lista já foi destruída\n");
		return;
	}

	t_nodo_t *aux = t->ini;
	t->atual = aux->prox;

	while (t->atual != t->fim->prox) {
		free (aux);
		aux = t->atual;
		t->atual = t->atual->prox;
	}
	free (aux);
	t->ini = NULL;
	t->fim = NULL;
	t->atual = NULL;
}

int lista_tiro_vazia (t_tiro *t) {
    if (t->ini->prox == t->fim)
        return 1;
    return 0;
}

int tamanho_tiro (t_tiro *t) {
    if (lista_tiro_vazia (t))
		return 0;

	return t->tamanho;
}

int insere_fim_tiro (t_tiro *t) {

	t_nodo_t * new;
	new = (t_nodo_t *) malloc (sizeof (t_nodo_t));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}

	t->atual = t->fim->prev;
	new->prev = t->atual;
	new->prox = t->fim;
	t->atual->prox = new;
	t->fim->prev = new;
    t->tamanho++;
	return 1;
}

int inicializa_atual_inicio_t (t_tiro *t) {
    if (lista_tiro_vazia (t))
        return 0;

    t->atual = t->ini->prox;
    return 1;
}

void incrementa_atual_tiro (t_tiro *t) {
    if (lista_tiro_vazia (t) || t->atual == t->fim)
        return;

    t->atual = t->atual->prox;
}

int consulta_atual_tiro (t_tiro *t) {
    if (lista_tiro_vazia (t)) {
        return 0;
	}

	if (t->atual == NULL)
		return 0;

	if (t->atual == t->ini || t->atual == t->fim)
		return 0;

	return 1;
}

int remove_atual_tiro (t_tiro *t) {
    if (lista_tiro_vazia (t))
		return 0;

	if (t->atual == NULL)
		return 0;

	if (t->atual == t->ini || t->atual == t->fim)
		return 0;

	t_nodo_t *aux = t->atual;
	t->atual->prox->prev = t->atual->prev;
	t->atual->prev->prox = t->atual->prox;
	free (aux);
	t->tamanho--;
	return 1;
}