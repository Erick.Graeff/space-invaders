#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include "lib_space.h"

int main () {
	t_lista l_alien, l_terra;
	t_barreira b;
	t_tiro tiro;

	inicializa_ncurses ();
	if (!tamanho_jogo (&l_alien, &l_terra, &tiro)) {
		endwin ();
		printf("O terminal deve ter no mínimo 38 linhas por 100 colunas\n");
		exit (0);
	}

	int u = 25000, key = getch ();

	inicializa_lista (&l_alien);
	inicializa_lista (&l_terra);
	inicializa_barreira (&b);
	inicializa_tiro (&tiro);
	prepara_jogo (&l_alien, &l_terra, &b);
	imprime_lista (&l_alien, &l_terra, &b, &tiro);
	int sentido = 1;
	int cont = 1, cont_desce = 1;
	int pontuacao = 0;/* 
	int cont_mae = 0; */
	while (1) {
		key = getch ();
		recebe_input (key, &l_alien, &l_terra, &b);

		/* move alien, retorna 0 caso tenha atingido alguma borda e desce a linha */
		if (!move_alien (&l_alien, sentido, &cont, /* &cont_mae,  */&cont_desce)) {
			muda_vel_alien (&l_alien, &u);
			sentido *= -1;
			cont = 1;
		}
		tiro_alien (&l_alien, &tiro);
		testa_colisao (&l_alien, &l_terra, &b, &tiro, &pontuacao);
		/* nave_mae (&l_alien); */
		imprime_lista (&l_alien, &l_terra, &b, &tiro);

		/* reinicia o jogo caso mate todos os aliens */
		if (lista_vazia (&l_alien)) {
			destroi_lista (&l_terra);
			destroi_barreira (&b);
			inicializa_lista (&l_alien);
			inicializa_lista (&l_terra);
			inicializa_barreira (&b);
			inicializa_tiro (&tiro);
			prepara_jogo (&l_alien, &l_terra, &b);
			usleep (u);
			imprime_lista (&l_alien, &l_terra, &b, &tiro);
			u -= 3000;
		}
		usleep (u);
	}
	
	return 0;
}
