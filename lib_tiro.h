#ifndef __LIB_TIRO__
#define __LIB_TIRO__

#include "lib_define.h"

struct t_nodo_t {
    int lin;
    int col;
    struct t_nodo_t *prox;
    struct t_nodo_t *prev;
};
typedef struct t_nodo_t t_nodo_t;

struct t_tiro {
    struct t_nodo_t *ini;
	struct t_nodo_t *atual;
	struct t_nodo_t *fim;
    int tamanho;
    int lin_max;
    int col_max;
    int cont;
};
typedef struct t_tiro t_tiro;

void imprime_tiros (t_tiro *t/* , t_lista *l */);

int inicializa_tiro (t_tiro *t);

void destroi_list_tiro (t_tiro *t);

int lista_tiro_vazia (t_tiro *t);

int tamanho_tiro (t_tiro *t);

int insere_fim_tiro (t_tiro *t);

int inicializa_atual_inicio_t (t_tiro *t);

void incrementa_atual_tiro (t_tiro *t);

int consulta_atual_tiro (t_tiro *t);

int remove_atual_tiro (t_tiro *t);

#endif