#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "lib_barreira.h"


void imprime_barreira (t_barreira *b) {
	t_nodo_b *aux;

	aux = b->ini->prox;
	while (aux != b->fim) {
		move (aux->lin, aux->col);
		addch (BARREIRA [0]);
		aux = aux->prox;
	}
}

int inicializa_barreira (t_barreira *b) {
	t_nodo_b *sent_ini, *sent_fim;

	sent_ini = (t_nodo_b *) malloc (sizeof (t_nodo_b));
	if (sent_ini == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}
	sent_fim = (t_nodo_b *) malloc (sizeof (t_nodo_b));
	if (sent_fim == NULL) {
		printf("Não há memória suficiente\n");
		return 0;
	}

	b->ini = sent_ini;
	b->fim = sent_fim;
	sent_ini->prox = b->fim;
	sent_fim->prev = b->ini;
	sent_ini->prev = NULL;
	sent_fim->prox = NULL;
	b->atual = NULL;
	return 1;
}

void destroi_barreira (t_barreira *b) {
	if (b->ini == NULL && b->fim == NULL && b->atual == NULL) {
		printf ("Lista já foi destruída\n");
		return;
	}

	t_nodo_b *aux = b->ini;
	b->atual = aux->prox;

	while (b->atual != b->fim->prox) {
		free (aux);
		aux = b->atual;
		b->atual = b->atual->prox;
	}
	free (aux);
	b->ini = NULL;
	b->fim = NULL;
	b->atual = NULL;
}

int insere_fim_barreira (t_barreira *b) {
	t_nodo_b * new;
	new = (t_nodo_b *) malloc (sizeof (t_nodo_b));
	if (new == NULL) {
		printf ("Não há memória suficiente. ");
		return 0;
	}

	b->atual = b->fim->prev;
	new->prev = b->atual;
	new->prox = b->fim;
	b->atual->prox = new;
	b->fim->prev = new;
	return 1;
}

void remove_barreira (t_nodo_b *b) {
	t_nodo_b *aux;

	aux = b;
	b->prev->prox = b->prox;
	b->prox->prev = b->prev;
	free (aux);
	return;
}