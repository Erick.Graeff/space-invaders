#ifndef __LIB_LISTA_SPACE__
#define __LIB_LISTA_SPACE__

#include "lib_barreira.h"
#include "lib_tiro.h"

struct t_nodo {
    int lin;
    int col;
    struct t_nodo *prox;
    struct t_nodo *prev;
    int tipo; /* 1 = alien; 2 = barreira; 3 = canhao; 4 = tiro; 5 = bomba */
    int alien; /* 1 = alien_1; 2 = alien_2; 3 = alien_3; 4 = mae */
    int estado;
    int vel;
};
typedef struct t_nodo t_nodo;

struct t_lista {
    int lin_max;
    int col_max;
    struct t_nodo *ini;
    struct t_nodo *atual;
    struct t_nodo *fim;
    int tamanho;
};
typedef struct t_lista t_lista;

void imprime_lista (t_lista *l_a, t_lista *l_t, t_barreira *b, t_tiro *t);

void imprime_bordas (t_lista *l);

void imprime_alien (t_lista *l, t_tiro *t);

void imprime_terra (t_lista *l, t_barreira *b);

int inicializa_lista (t_lista *l);

int lista_vazia (t_lista *l);

void destroi_lista (t_lista *l);

int tamanho_lista (t_lista *l);

int insere_fim_lista (int item, t_lista *l);

int inicializa_atual_inicio (t_lista *l);

int inicializa_atual_fim (t_lista *l);

void incrementa_atual (t_lista *l);

void decrementa_atual (t_lista *l);

int consulta_item_atual (t_lista *l);

int remove_item_atual (t_lista *l);

#endif