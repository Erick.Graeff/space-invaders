#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <unistd.h>
#include <time.h>
#include "lib_space.h"
#include "lib_define.h"

void inicializa_ncurses () {
    initscr ();              /* inicia a tela */
    cbreak ();               /* desabilita o buffer de entrada */
    noecho ();               /* não mostra os caracteres digitados */
    nodelay (stdscr, TRUE);  /* faz com que getch não aguarde a digitação */
    keypad (stdscr, TRUE);   /* permite a leitura das setas */
    curs_set (FALSE);        /* não mostra o cursor na tela */

	start_color();
	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_RED, COLOR_BLACK);
}

void recebe_input (int key, t_lista *l_a, t_lista *l_t, t_barreira *b) {
	if (key == ' ') {
    	atira (l_t);
	}
	else if (key == KEY_LEFT)
		mv_canhao_esq (l_t);
	
	else if (key == KEY_RIGHT)
		mv_canhao_dir (l_t);
	
	else if (key == 'q') {
		destroi_lista (l_a);
		destroi_lista (l_t);
		destroi_barreira (b);
		endwin();
		exit(0);
	}
}

int tamanho_jogo (t_lista *l_a, t_lista *l_t, t_tiro *t) {

	/* Minimo de linhas e colunas necessarias */
	int i, j;
	getmaxyx (stdscr, i, j);
	l_a->lin_max = 38;
	l_a->col_max = 100;

	if (i < l_a->lin_max || j < l_a->col_max)
		return 0;

	l_t->lin_max = l_a->lin_max;
	l_t->col_max = l_a->col_max;
	t->lin_max = l_a->lin_max;
	t->col_max = l_a->col_max;
	return 1;
}

void atira (t_lista *l) {
	
	/* insere o tiro no fim da lista e passa a coordenada que ele deve ser inserido com base na coordenada do canhao */
	if (tamanho_lista (l) <= 4) {
		insere_fim_lista (4, l);
		l->fim->prev->lin = l->ini->prox->lin - 1;
		l->fim->prev->col = l->ini->prox->col + 2;
	}
}

void mv_canhao_dir (t_lista *l) {
	if (l->ini->prox->col + 5 == l->col_max)
		return;

	l->ini->prox->col++;
}

void mv_canhao_esq (t_lista *l) {
	if (l->ini->prox->col - 1 == 0)
		return;

	l->ini->prox->col--;
}

void list_alien_1 (t_lista *l, int x, int *y, int v) {
	int cont, max_alien = 11;

	cont = 1;
	inicializa_atual_inicio (l);
	while (cont <= max_alien) {
		cont++;
		insere_fim_lista (1, l);
		/* informa a posicao inicial de cada alien */
		l->fim->prev->lin = *y;
		l->fim->prev->col = x;
		l->fim->prev->alien = 1;
		l->fim->prev->vel = v;
		l->fim->prev->estado = 1;
		x += 7;
	}
	*y += 4;
}

void list_alien_2 (t_lista *l, int x, int *y, int v) {
	int cont, max_alien = 11;

	cont = 1;
	inicializa_atual_fim (l);
	while (cont <= max_alien) {
		cont++;
		insere_fim_lista (1, l);
		/* informa a posicao inicial de cada alien */
		l->fim->prev->lin = *y;
		l->fim->prev->col = x;
		l->fim->prev->alien = 2;
		l->fim->prev->vel = v;
		l->fim->prev->estado = 1;
		x += 7;
	}
	*y += 4;
}

void list_alien_3 (t_lista *l, int x, int *y, int v) {
	int cont, max_alien = 11;

	cont = 1;
	inicializa_atual_fim (l);
	while (cont <= max_alien) {
		cont++;
		insere_fim_lista (1, l);
		/* informa a posicao inicial de cada alien */
		l->fim->prev->lin = *y;
		l->fim->prev->col = x;
		l->fim->prev->alien = 3;
		l->fim->prev->vel = v;
		l->fim->prev->estado = 1;
		x += 7;
	}
	*y += 4;
}

void list_t (t_lista *l, t_barreira *b, int x, int y) {
	int cont, max = 4;

	cont = 0;
	inicializa_atual_inicio (l);
	/* informa posicao inicial do canhao */
	insere_fim_lista (3, l);
	l->fim->prev->lin = l->lin_max - 2;
	l->fim->prev->col = l->col_max / 2 - 2;
	l->fim->prev->estado = 1;

	int i = 0;
	int j = 0;
	while (cont < max) {
		cont++;
		for (i = y; i < y + 3; i++)
			for (j = x; j < x + 7; j++) {
				insere_fim_barreira (b);
				/* informa a posicao inicial de cada barreira */
				b->fim->prev->lin = i;
				b->fim->prev->col = j;

			}
		x += 21;
	}
}

void prepara_jogo (t_lista *l_alien, t_lista *l_terra, t_barreira *b) {
	int pos_y;

	/* configuracoes iniciais dos aliens de cada tipo */
	pos_y = 6;
	
	/* informa tambem a posicao dos alien, cada chamada da funcao corresponde a uma linha de aliens */
	list_alien_3 (l_alien, 13, &pos_y, 15);
	list_alien_2 (l_alien, 13, &pos_y, 15);
	list_alien_2 (l_alien, 13, &pos_y, 15);
	list_alien_1 (l_alien, 13, &pos_y, 15);
	list_alien_1 (l_alien, 13, &pos_y, 15);

	/* barreira e nave */
	list_t (l_terra, b, 15, l_alien->lin_max - 6);
}

int move_alien (t_lista *l, int sentido, int *cont, /* int *cont_mae,  */int *desce) {
	
	/* t_nodo *aux = l->fim->prev; */
	/* *cont_mae += 1;
	if (aux->alien == 4 && *cont % (l->ini->prox->vel / 2) == 0)
		aux->col++; */

	inicializa_atual_inicio (l);
	*cont += 1;
	if (*cont % l->ini->prox->vel == 0) {
		while (consulta_item_atual (l) && l->atual->tipo == 1/*  && l->atual->alien != 4 */) {
			/* caso atinja uma das bordas desce linha */
			if (((ultimo_alien (l) + 5 == l->col_max) && (sentido > 0)) || ((primeiro_alien (l) - 1 == 0) && (sentido < 0))) {
				/* para garantir que entre na primeira condicao */
				*cont -= 1;
				return !(desce_linha (l, desce));
			}	

			l->atual->col += sentido;
			incrementa_atual (l);
		}
		*cont = 1;
	}
	return 1;
}

int desce_linha (t_lista *l, int *desce) {
	inicializa_atual_inicio (l);
	t_nodo *aux;
	aux = l->ini->prox;
	*desce += 1;
	if (*desce % aux->vel == 0) {
		while (consulta_item_atual (l) && l->atual->tipo == 1) {
			/* caso atinja a ultima linha */
			if (ultima_linha (l) + 4 == l->lin_max) {
				endwin ();
				exit (0);
			}

			l->atual->lin++;
			incrementa_atual (l);
		}
		return 1;
	}
	return 0;
}

/* verifica qual eh o alien mais a esquerda */
int ultimo_alien (t_lista *l) {
    int ultimo = 0;
	t_nodo *aux = l->atual;
    while (aux != l->fim && aux->tipo == 1) {
        if (aux->col > ultimo)
            ultimo = aux->col;
        aux = aux->prox;
    }
    return ultimo;
}

/* verifica qual eh o alien mais a direita */
int primeiro_alien (t_lista *l) {
    int primeiro = l->col_max;
	t_nodo *aux = l->atual;
    while (aux != l->fim) {
        if (aux->col < primeiro)
            primeiro = aux->col;
        aux = aux->prox;
    }
    return primeiro;
}

/* verifica qual eh a ultima linha de aliens */
int ultima_linha (t_lista *l) {
	int ultima = 0;
	t_nodo *aux = l->atual;
	while (aux != l->fim) {
		if (aux->lin > ultima)
			ultima = aux->col;
		aux = aux->prox;
	}
	return ultima;
}

void muda_vel_alien (t_lista *l, int *u) {
	t_nodo *aux = l->atual;
	while (aux != l->fim) {
		aux->vel -= 2;
		aux = aux->prox;
	}
	*u -= 1500;
}

void testa_colisao (t_lista *l_a, t_lista *l_t, t_barreira *b, t_tiro *t, int *pont) {
	/* para cada tiro, sera verificado se ele atingiu algum alien ou barreira  */

	inicializa_atual_inicio (l_t);
	while (consulta_item_atual (l_t)) {
		inicializa_atual_inicio (l_a);
		while (consulta_item_atual (l_a)) {
			/* caso seja um tiro do canhao */
			if (l_t->atual->tipo == 4) {
				if (colide_alien_tiro (l_a->atual, l_t->atual) || colide_barreira_tiro (b, l_t->atual))
					remove_item_atual (l_t);
			}
			/* testa as colisoes entre cada alien e as barreiras */
			colide_alien_barreira (l_a->atual, b);
			incrementa_atual (l_a);
		}
		incrementa_atual (l_t);
	}
	inicializa_atual_inicio_t (t);
	while (consulta_atual_tiro (t)) {
		if (colide_bomba_barreira (t->atual, b))
			remove_atual_tiro (t);

		if (colide_bomba_canhao (t->atual, l_t->ini->prox)) {
			remove_atual_tiro (t);
			endwin ();
			printf ("Voce falhou e os aliens conseguiram invadir a Terra\n");
			exit (0);
		}
		incrementa_atual_tiro (t);
	}
}

int colide_alien_tiro (t_nodo *alien, t_nodo *tiro) {
	int i, j;

	for (i = alien->lin; i < alien->lin + 3; i++)
		for (j = alien->col; j < alien->col + 5; j++) {
			if ((i == tiro->lin) && (j == tiro->col)){
				alien->estado = MORRENDO;
				return 1;
			}
		}
	return 0;
}

int colide_barreira_tiro (t_barreira *b, t_nodo *tiro) {
	t_nodo_b *aux;

	aux = b->ini->prox;
	while (aux != b->fim) {
		if (aux->lin == tiro->lin && aux->col == tiro->col) {
			remove_barreira (aux);
			return 1;
		}

		aux = aux->prox;
	}
	return 0;
}

void colide_alien_barreira (t_nodo *alien, t_barreira *b) {
	t_nodo_b *aux;
	int i, j;

	/* para cada caracter do alien, verifica se ele encostou em algum da barreira */
	for (i = alien->lin; i < alien->lin + 3; i++)
		for (j = alien->col; j < alien->col + 5; j++) {
			aux = b->ini->prox;
			while (aux != b->fim) {
				if (i == aux->lin && j == aux->col)
					remove_barreira (aux);
				aux = aux->prox;
			}
		}
}

int colide_bomba_barreira (t_nodo_t *tiro, t_barreira *b) {	
	t_nodo_b *aux;

	aux = b->ini->prox;
	while (aux != b->fim) {
		if (aux->lin == tiro->lin && aux->col == tiro->col) {
			remove_barreira (aux);
			return 1;
		}

		aux = aux->prox;
	}
	return 0;
}

int colide_bomba_canhao (t_nodo_t *tiro, t_nodo *canhao) {
	int i, j;
	for (i = canhao->lin; i < canhao->lin + 2; i++)
		for (j = canhao->col; j < canhao->col + 5; j++) {
			if (tiro->lin == canhao->lin && tiro->col == canhao->col) {
				canhao->estado = MORRENDO;
				return 1;
			}
		}
	return 0;
}

void tiro_alien (t_lista *l, t_tiro *t) {
	/* seleciona o alien que ira atirar */
	srand (time (NULL));
	int tam = tamanho_lista (l);
	int alien = 1 + (rand () % tam);
	if (alien % FREQ_TIRO == 0 && tamanho_tiro (t) < 1) {
		int i = 1;
		inicializa_atual_inicio (l);
		while (i <= alien) {
			incrementa_atual (l);
			i++;
		}
		/* passa as coordenadas do tiro com base no alien */
		insere_fim_tiro (t);
		t->fim->prev->lin = l->atual->lin + 3;
		t->fim->prev->col = l->atual->col + 2;
		return;
	}
}

/* void nave_mae (t_lista *l) {
	srand (time (NULL));
	int mae = rand () % 100;
	if (mae % FREQUENCIA_MAE == 0) {
		insere_fim_lista (1, l);
		l->fim->prev->alien = 4;
		l->fim->prev->lin = 2;
		l->fim->prev->col = 1;
	}
} */