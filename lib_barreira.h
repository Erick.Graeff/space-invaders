#ifndef __LIB_BARREIRA__
#define __LIB_BARREIRA__

#include "lib_define.h"

struct t_nodo_b {
	int lin;
	int col;
	struct t_nodo_b *prox;
	struct t_nodo_b *prev;
};
typedef struct t_nodo_b t_nodo_b;

struct t_barreira {
	struct t_nodo_b *ini;
	struct t_nodo_b *atual;
	struct t_nodo_b *fim;
};
typedef struct t_barreira t_barreira;

void imprime_barreira (t_barreira *b);

int inicializa_barreira (t_barreira *b);

void destroi_barreira (t_barreira *b);

int insere_fim_barreira (t_barreira *b);

void remove_barreira (t_nodo_b *b);

#endif